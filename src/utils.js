const fs = require("fs");;
const path = require("path");;
const moment = require("moment");;
const os = require("os");;

const Logger = require("./logger");
const fetcher = require("./modules/fetchers/axios");
const parser = require("./modules/parsers/jsonPath");

const isOperators = (str) => ["<", ">", "&", "|", "="].includes(str);

const replaceOperator = (op) => {
  if (op === "&") { return "&&"; }
  if (op === "|") { return "||"; }
  if (op === "=") { return "=="; }
  return op;
};

const asPromise = (v) => Promise.resolve(v);

const hasSensor = (probeName, probes) => probes.filter((p) => p.name === probeName).length !== 0;

const getProbeValue = async (name, probes) => {
  const probe = probes.filter((s) => s.name === name)[0];
  const response = await fetcher.process(probe.url);
  const parsed = await parser.process(response, probe.target);
  return parsed;
};


const resolveExpr = async (state, probes) => {
  if (state.condition === undefined) { return true; }
  const splitted = state.condition.split(" ");
  const promises = splitted.map((exprPart) => {
    // It's an operator
    if (isOperators(exprPart)) { return asPromise(replaceOperator(exprPart)); }
    // It's a comparison value
    if (parseInt(exprPart, 10) || parseInt(exprPart, 10) === 0) { return asPromise(exprPart); }
    // This is a word without corresponding sensor
    if (!hasSensor(exprPart, probes)) {
      Logger.error(`[${JSON.stringify(state)}] ${exprPart} has no provided sensor.`);
      return asPromise(null);
    }
    // Fetch the probe value
    return getProbeValue(exprPart, probes);
  });
  const parts = await Promise.all(promises);
  // Condition not resolved
  if (parts.some((p) => p === null)) { return null; }
  return parts.join(" ");
};

const isTodayAnException = (dates) => dates.some(date => moment().isSame(moment(date, 'DD.MM.YYYY'), 'day'));

const alreadyLaunched = (actionName) => {
  const filename = path.resolve(os.tmpdir(), `${actionName}_${moment().format("DDMMYYYY")}`);
  return fs.existsSync(filename);
};

const setLaunchedToday = (actionName) => {
  const filename = path.resolve(os.tmpdir(), `${actionName}_${moment().format("DDMMYYYY")}`);
  const exists = fs.existsSync(filename);
  if (!exists) { fs.writeFileSync(filename, moment().format()); }
};

module.exports = {
  isOperators,
  replaceOperator,
  asPromise,
  hasSensor,
  getProbeValue,
  resolveExpr,
  isTodayAnException,
  alreadyLaunched,
  setLaunchedToday
};
