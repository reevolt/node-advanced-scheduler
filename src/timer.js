class DelayedTimer {
    constructor(delay) {
        this.delay = delay;
        this.queue = [];
    }
    takeLatest(fn) {
        this.cancelAll();
        this.queue.push(setTimeout(fn, this.delay));
    }
    cancelAll() {
        clearTimeout(this.queue.pop());
    }
}

module.exports = DelayedTimer;