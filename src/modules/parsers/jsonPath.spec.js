const { expect } = require("chai");

const { process } = require("./jsonPath");

describe("json path parser", () => {
  it("should contains a function called process", () => {
    expect(process).to.be.a("function");
  });

  it("should return null when bad expression", async () => {
    const data = { a: 2 };
    const args =  "$.b";
    const v = await process(data, args);
    expect(v).to.equal(null);
  });

  it("should parse simple expression" ,async () => {
    const data = { a: 2 };
    const args = "$.a";
    const v = await process(data, args);
    expect(v).to.equal(2);
  });

  it("should parse array expression", async () => {
    const data = [{ a: [["a", "b"], ["c", "d"]] }];
    const args = "$.[0].a[-1:][0]" ;
    const v = await process(data, args);
    expect(v).to.equal("c");
  });
});
