const get = require("lodash/get");

const process = (data, args) => Promise.resolve(get(data, args.expression, null));

module.exports = {
    process
}
