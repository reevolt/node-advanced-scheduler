const { JSONPath } = require("jsonpath-plus");

const process = (json, path) => 
  new Promise((callback) => JSONPath({path, json, callback}));

module.exports = {
  process
}