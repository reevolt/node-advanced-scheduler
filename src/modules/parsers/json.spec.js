const { expect } = require("chai");

const parser = require("./json");

describe("json parser", () => {
  it("should contains a function called process", () => {
    expect(parser.process).to.be.a("function");
  });
  it("should parse simple expression", (done) => {
    const p = parser.process;
    const data = { a: 2 };
    const args = { expression: "a" };
    p(data, args).then((v) => {
      expect(v).to.equal(2);
      done();
    });
  });
  it("should return null when bad expression", (done) => {
    const p = parser.process;
    const data = { a: 2 };
    const args = { expression: "b" };
    p(data, args).then((v) => {
      expect(v).to.equal(null);
      done();
    });
  });
});
