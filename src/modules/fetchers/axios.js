const axios = require("axios");;

const process = async (args) => {
  const response = await axios(args);
  return response.data;
};

module.exports = {
  process
}