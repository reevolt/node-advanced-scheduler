const cron = require("node-cron");
const safeEval = require("safe-eval");

const Logger = require("./logger");
const { resolveExpr, isTodayAnException, alreadyLaunched, setLaunchedToday } = require("./utils");
const { processHttp } = require("./client");

const { DRY_RUN } = process.env;

const crons = [];

const registerAction = (state, probes, action) => {

  if (!cron.validate(state.cron)) {
    Logger.fromAction(state.name)(`has an INVALID cron: ${state.cron}`);
    return;
  }
  Logger.fromAction(state.name)(`has an valid cron: ${state.cron}`);

  const job = () => {

    Logger.fromAction(state.name)(`Starting job`);

    const todayExcluded = isTodayAnException(state.except || []);
    if (todayExcluded) {
      Logger.fromAction(state.name)('Today is a special day, do not run action');
      return;
    }

    const launched = (state.only === 'once_per_day' && alreadyLaunched(state.name));
    if (launched) {
      Logger.fromAction(state.name)('Already launched today');
      return;
    }

    resolveExpr(state, probes).then((evaluatedExpr) => {
      if (!evaluatedExpr) {
        Logger.fromAction(state.name)("Condition not resolved");
        Logger.fromAction(state.name)(` - evaluated expression: ${evaluatedExpr}`);
        return false;
      }

      const conditionResult = safeEval(evaluatedExpr);

      Logger.fromAction(state.name)(`Condition result: ${conditionResult}`);
      Logger.fromAction(state.name)(` - condition: ${state.condition || 'not defined'}`);
      Logger.fromAction(state.name)(` - evaluated: ${evaluatedExpr}`);

      if (conditionResult) {
        setLaunchedToday(state.name);
        processHttp(action.url, action.method);
      }
    });
  };

  if (DRY_RUN === 'true') {
    job();
  } else {
    Logger.fromAction(state.name)(`Registering with cron ${state.cron}`);
    // Delay action of 20s to wait the latest grafana data
    crons.push(cron.schedule(state.cron, () => setTimeout(() => job(), 20000)));
  }

};

const clearJobs = () => {
  Logger.info("Stopping all cron...");
  crons.forEach(c => c.stop());
}

module.exports = {
  registerAction,
  clearJobs
}