const debug = require("debug");;

class Logger {
  static error(str) { return debug("app:error")(str); }

  static info(str) { return debug("app:info")(str); }

  static fromAction(actionName) { return (str) => debug(`app:${actionName}`)(str); }
}

module.exports = Logger;