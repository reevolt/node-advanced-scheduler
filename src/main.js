const fs = require("fs");
const path = require("path");
const yaml = require("js-yaml");

const scheduler = require("./scheduler");
const Logger = require("./logger");
const DelayedTimer = require("./timer");

const cronFile = path.resolve(__dirname, "../jobs.yml");

const delayedReload = new DelayedTimer(1000);

fs.watch(cronFile, () => delayedReload.takeLatest(setupCrons));

const setupCrons = () => {
  const file = fs.readFileSync(cronFile).toString("utf-8");
  const { states, probes, actions } = yaml.load(file);
  scheduler.clearJobs();

  states.forEach(s => {
    if (actions.filter(a => a.name === s.action).length === 0) {
      Logger.error(`'${s.name}': action '${s.action}' not found`)
      process.exit(-1);
    }
  });

  states.forEach((state) => {
    const action = actions.filter((a) => a.name === state.action)[0];
    scheduler.registerAction(state, probes, action);
  });

};

setupCrons();