const axios = require("axios");;
const Logger = require("./logger");

const processHttp = async (url, method) => {
  try {
    const response = await axios({ url, method });
    return response;
  } catch (err) {
    Logger.error({
      url, err: err.errno
    });
  }
};

module.exports = {
  processHttp
}