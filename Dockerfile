FROM arm32v7/node:16.3.0-stretch-slim

WORKDIR /app

COPY . /app

ENV NODE_ENV production

RUN npm install --production

ENV DEBUG app:*

ENTRYPOINT ["node", "src/main.js"]
